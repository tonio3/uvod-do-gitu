﻿using StringManipulationConsoleApp.CustomExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringManipulationConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter string which will be convereted to CamelCase.");

            var text = Console.ReadLine();

            Console.WriteLine("Original word: " + text);
            Console.WriteLine("CamelCase word: " + text.ToCamelCase());
        }
    }
}
