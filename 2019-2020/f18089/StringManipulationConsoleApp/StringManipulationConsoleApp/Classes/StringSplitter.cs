﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StringManipulationConsoleApp.Classes
{
    class StringSplitter
    {
        public static string[] Split(string text, StringType type)
        {
            if (StringType.Regular == type) return text.Split(StringTypesDelimiters.RegularCaseDelimiter, '\t');
            if (StringType.KebabCase == type) return text.Split(StringTypesDelimiters.KebabCaseDelimiter);
            if (StringType.SnakeCase == type) return text.Split(StringTypesDelimiters.SnakeCaseDelimiter);
            if (StringType.PascalCase == type) return SplitCamelCase(text);

            return text.Split(StringTypesDelimiters.RegularCaseDelimiter, '\t');
        }

        private static string[] SplitCamelCase(string text)
        {
            return Regex.Split(text, @"(?<!^)(?=[A-Z])");
        }
    }
}
