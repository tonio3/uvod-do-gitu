# camelCase řešení - Škrabánek

přidal jsem vlastní řešení napsané v **PHP** s využití knihovny **[Bootstrap](https://getbootstrap.com/)**

Aplikace se chová jako *"generátor názvů metod ve světě programování"*

## Soubory
- index.php -> hlavní stránka obsahující pole a list vložených _"citací"_
- postik.php -> php skript, který vyváří jednotlivé "citace"
- list.txt -> soubor, který ukládá veškeré "citace" *(pokud není vytvořen, PHP skript to vytvoří)*
	- **camelCase**("text",bool velkePismeno = false) -> metoda ktera vraci **$string** upravený na camelCase
		- bool velke Pismeno -> TRUE/FALSE -> moznost nastavení prvního písmena na Velké nebo malé

