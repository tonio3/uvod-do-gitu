import re

pattern = re.compile("[^A-Za-z]+")
# regex pattern

def to_camel(chars):
  words = re.split(pattern, chars)

  if len(words) == 1:
     words = re.findall(r'[A-Z](?:[a-z]+|[A-Z]*(?=[A-Z]|$))', chars)
  # regex for PascalCase split

  return "".join(w.lower() if i is 0 else w.title() for i, w in enumerate(words))

# fce test call
test1 = to_camel("hello world")
test2 = to_camel("hello_world")
test3 = to_camel("hello-world")
test4 = to_camel("HelloWorld")
print(test1 + " " + test2 + " " + test3 + " " + test4)
