import Data.List.Split
import Data.Char (toUpper, toLower)

main = do
    putStrLn "Write any sentence. It's going to be transformed to camelCase."
    do
        sentence <- getLine
        putStrLn ("camelCase sentece: " ++ toCamelCase sentence)
        do
            putStrLn ("Do you wish to exit? Y/N")
            do
                exitCode <- getLine
                if toLower(head exitCode) == 'n'
                    then main
                    else return() 


toCamelCase:: [Char] -> [Char]
toCamelCase input = toLower (head x) : tail x
    where x = concat (map (\ x -> toUpper( head x) : tail x ) (splitOn " " input))